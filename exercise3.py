# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class CreateIssuesTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://jira.atlassian.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_create_an_issue(self):
	"""
	As the test requirement need completed in 1~1.5hours,
	Here is just testing a process of create issue. Actually, all the
	composition of parameters should be tested.
	"""        

	driver = self.driver
	
	# Log in
        driver.get(self.base_url + "/secure/Dashboard.jspa")
        driver.find_element_by_link_text("Log In").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("yuanxiaonz@yahoo.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("atlassian")
        driver.find_element_by_id("login-submit").click()
        
	# Create issue
	driver.find_element_by_id("create_link").click()

	# Summary
        driver.find_element_by_id("summary").clear()
        driver.find_element_by_id("summary").send_keys("improvement_test")

	# Security
        Select(driver.find_element_by_id("security")).select_by_visible_text("Reporter and developers")

	# Priority	
        driver.find_element_by_xpath("//div[@id='priority-single-select']/span").click()
        driver.find_element_by_link_text("Critical").click()
        
	# Components
	driver.find_element_by_xpath("//div[@id='components-multi-select']/span").click()
        driver.find_element_by_link_text("Component 1").click()
        
	# Version
	driver.find_element_by_xpath("//div[@id='versions-multi-select']/span").click()
        driver.find_element_by_link_text("Test Version 1").click()
        
	# Environment
	driver.find_element_by_id("environment").clear()
        driver.find_element_by_id("environment").send_keys("Linux")
        
	# Description
	driver.find_element_by_id("description").clear()
        driver.find_element_by_id("description").send_keys("description")
        
	driver.find_element_by_id("customfield_10010").clear()
        driver.find_element_by_id("customfield_10010").send_keys("randowm text")
        
        Select(driver.find_element_by_id("customfield_10040")).select_by_visible_text("Microsoft")
        driver.find_element_by_id("customfield_10064-4").click()
        
	# Time tracking
	driver.find_element_by_id("timetracking").clear()
        driver.find_element_by_id("timetracking").send_keys("3w")
        
	Select(driver.find_element_by_id("customfield_10061")).select_by_visible_text("Birds")
        Select(driver.find_element_by_id("customfield_10061")).select_by_visible_text("Monotremes")
        Select(driver.find_element_by_id("customfield_10061:1")).select_by_visible_text("Platypus")
        
	# Test name
	driver.find_element_by_id("customfield_10530").clear()
        driver.find_element_by_id("customfield_10530").send_keys("xiao")
       
	# My single Version Picker
 	Select(driver.find_element_by_id("customfield_10550")).select_by_visible_text("Test Version 1")
        
        # Stroy Point 
	driver.find_element_by_id("customfield_10653").clear()
        driver.find_element_by_id("customfield_10653").send_keys("5")
         
	driver.find_element_by_id("customfield_14130").clear()
        driver.find_element_by_id("customfield_14130").send_keys("no")
        
	# Submit
	driver.find_element_by_id("create-issue-submit").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
